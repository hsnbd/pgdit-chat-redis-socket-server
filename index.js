const server = require("http").Server();

const Redis = require("ioredis");

const redis = new Redis(6379);

// Create a new Socket.io instance
const io = require('socket.io')(server);

redis.psubscribe('*');

redis.on('pmessage', function (pattern, channel, message) {
    message = JSON.parse(message);
    io.emit('message_'+ message.data.message.batch_id, message.data.message)
});
server.listen(3000);
